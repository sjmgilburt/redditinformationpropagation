# Information Propagation in Reddit Threads

This project was completed in May 2015 by Samuel J. M. Gilburt, Fang Nok Hang and Ivan Poteryakhin as part of an 
assignment for the CSC4641 Social Information Network Analysis and Engineering course at Hong Kong University of 
Science and Technology (HKUST).

The project is summarily described below, however the [full report](Information Propagation in Reddit Threads.pdf) 
is available in the repository and explains in full detail the purpose, methodology and results of the project.

## Summary

### Purpose

The intention of this project was to explore the emergence and propagation of topics, themes and opinions within and 
across discussion threads on the popular aggregator forum [Reddit](http://www.reddit.com), and to investigate whether 
particular established ideas are more likely to propagate than new original thoughts and posts.

### Methodology

Unlike [Twitter](http://www.twitter.com), where trends can be tracked using hashtags or retweets, Reddit has no such 
built-in mechanism to track influence. As a result, the this project used a software package called 
[Wandora](http://wandora.org) to trawl Reddit comments and build a dataset that would allow comment contents to be 
semantically analysed.

Firstly, the comments were extracted via Wandora, and then parsed using a custom HTML parser to extract the comment ID,
body and timestamp of each data point. As comments with URLs were found to adversely affect semantic analysis, these 
were removed from comment content.

Secondly, all comments in a thread were pairwise compared using a Natural Language Processing (NLP) method, which 
scored pairs from 0 to 5 on similarity. All threads analysed showed some of the expected behaviour - either direct or 
semantic copying of comment content.

Thirdly, these comments were again parsed to output the relationships between the comment as HTML that could be mapped 
onto a Wandora graph. The produced graph shows the direction of information diffusion amongst comments across the 
threads, with older comments taken to influence newer ones. Sometimes this influencing was as a result of a recurring 
theme in a conversation, or as the result of a person deliberately copying someone else (or themselves) in an entirely 
different part of the conversation thread.

### Results

Below is a sample of the results, analysing four threads from four different subreddits, and drawing conclusions from 
the produced graphs.

#### /r/circlejerk

This subreddit is a parody subreddit where users mock common themes on the site. Posts in /r/circlejerk intentionally 
repeat similar jokes or memes. As such, there is a large number of similar posts, and we also see very high clustering 
of comments in the graph:

![A graph of an /r/circlejerk thread](./images/circlejerk.png)

#### /r/funny

Users on this board post puns more than serious discussion. People often repost puns elsewhere in the thread in an 
attempt to pass them off as their own, and as a result we see many connections across subtrees, with generally low 
clustering.

![A graph of an /r/funny thread](./images/funny.png)

#### /r/politics

Here there are much fewer clusters and copied comments in general, as one might expect from a politics-based discussion 
thread. More people in this subreddit post their own more complex and original thoughts, and many of the "copied" posts 
are in fact properly quoting from elsewhere in the thread, but giving credit to the original author.

![A graph of an /r/politics thread](./images/politics.png)


#### /r/todayilearned

Most posts engage in serious discussion on the topic. In the highlighted box below, there is an example of when posts 
become less serious (as is common in many threads across Reddit). These posts are repetition of a Korean 
["fan death"](https://en.wikipedia.org/wiki/Fan_death) meme, leading to this cluster forming.

![A graph of an /r/todayilearned thread](./images/todayilearned.png)

For more detail, please see the [full report](Information Propagation in Reddit Threads.pdf) in the repository.

## Built with

* [Wandora](http://wandora.org) - trawling and extraction of Reddit comment data

## Acknowledgements

* [Bluetip Design](https://thenounproject.com/bluetip) - project icon
