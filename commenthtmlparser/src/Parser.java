/** COMP4641 Project
 * 	Group 14
 *  Author: Samuel Gilburt, 20254712
 *  Date: 05 May 2015
 *  Version: 1.0
 */

import java.io.*;
import org.jsoup.*;
import org.jsoup.nodes.*;

public class Parser {
	private static File dir = new File("htmls/");
	
	private static File[] files = dir.listFiles();
	private static int fileNo = dir.listFiles().length;
	
	private static String id;
	private static String body;
	private static String time;
	
	private static final String DELIM = "@#@";
	
	private static File outFile = new File("comments.txt");

	public static void main(String[] args) {		
		for (int i=0; i<fileNo; i++) {
			try {
				Document html = Jsoup.parse(files[i], null);
				
				id = html.getElementsByClass("topiccurrent").first().text().substring(9, 19);				
				body = html.getElementsByClass("topicnormal").first().text();
				body = body.replaceAll("https?://\\S+\\s?", "");
				time = html.getElementsByClass("topicnormal").get(1).text().substring(0, 19);
			}
			catch (IOException e) {
				e.printStackTrace();
			}
			
			if (!(body.equals("[deleted]") || body.equals(""))) {
				try {
					PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter(outFile, true)));
				    out.println(id + DELIM + body + DELIM + time);
				    out.close();
				}
				catch (IOException e) {
				    e.printStackTrace();
				}
			}
		}
	}
}
