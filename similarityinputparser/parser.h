#ifndef PARSER_H_
#define PARSER_H_

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

class parser{
public:
	parser(string scorefilename, string timefilename, string inputhtml, string outputhtml, string delimit, double thresh);
	struct CommentTable{
		string stringID;
		int intID;
		string timestamp;
	};
	vector<CommentTable*> cTable;
	void InitTable();
	void ParseToWandora();

private:
	double threshold;
	string scoreFilename;
	string timestampFilename;
	string inputhtmlFilename;
	string outputhtmlFilename;
	string delimiter;
	void InitIDs();
	void InitTimestamp();
	int FindByCommentID(string commentID);
	void PrintTable();
	int FindLineOfLastStringInstance(string s, string filename);

};

#endif /* PARSER_H_ */
