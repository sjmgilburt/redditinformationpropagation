#include "parser.h"

parser::parser(string scorefilename, string timefilename, string inputhtml, string outputhtml, string delimit, double thresh){
	scoreFilename = scorefilename;
	timestampFilename = timefilename;
	inputhtmlFilename = inputhtml;
	outputhtmlFilename = outputhtml;
	delimiter = delimit;
	threshold = thresh;
}

void parser::InitTable(){
	InitIDs();
	InitTimestamp();
}

void parser::InitIDs(){
	string line;
	ifstream myfile (inputhtmlFilename.c_str());
	string ctmp = "Comment++%28";
	string itmp = "node";
	if (myfile.is_open()){
		while ( getline (myfile,line) ){
			size_t pos = line.find(ctmp);
			if(pos!=std::string::npos){
				CommentTable * entry = new CommentTable();
				string commentID = line.substr(pos+ctmp.length(),10);
				entry->stringID = commentID;
				getline (myfile, line);
				size_t pos2 = line.find(itmp);
				string tmpID = line.substr(pos2+itmp.length(), line.length() -1 - pos2 - itmp.length());
				entry->intID = atoi(tmpID.c_str());
				cTable.push_back(entry);
			}
		}
		myfile.close();
	}
	else cout << "Unable to open file";
}

void parser::InitTimestamp(){
	string line;
	ifstream myfile (timestampFilename.c_str());
	string del = delimiter;
	if (myfile.is_open()){
		while ( getline (myfile,line) ){
			size_t pos = line.find(del);
			if(pos==std::string::npos) cout<<"cannot find"<<endl;
			size_t pos2 = line.find(del,pos+1);
			if(pos2==std::string::npos) cout<<"cannot find"<<endl;
			string commentID = line.substr(0,pos);
			string timestmp = line.substr(pos2+del.length());
			timestmp.replace(timestmp.find("T"), 1,"");
			timestmp.replace(timestmp.find(":"), 1,"");
			timestmp.replace(timestmp.find(":"), 1,"");
			timestmp.replace(timestmp.find("-"), 1,"");
			timestmp.replace(timestmp.find("-"), 1,"");
			int entrypos = FindByCommentID(commentID);

			cTable[entrypos]->timestamp = timestmp;
		}
		myfile.close();
	}
	else cout << "Unable to open file";

	//PrintTable();
}

int parser::FindByCommentID(string commentID){
	for(vector<CommentTable*>::iterator p = cTable.begin();p!=cTable.end();p++){
		if((*p)->stringID.compare(commentID)==0){
			return p - cTable.begin();
		}
	}
	return -1;
}

void parser::PrintTable(){
	for(vector<CommentTable*>::iterator p = cTable.begin();p!=cTable.end();p++){
		cout<<(*p)->stringID+"\t"<<(*p)->intID<<"\t"+(*p)->timestamp<<endl;
	}
}

void parser::ParseToWandora(){
	string line, line2;
	ifstream myfile (scoreFilename.c_str());
	ifstream myfile2 (inputhtmlFilename.c_str());
	ofstream outfile(outputhtmlFilename.c_str());
	string del = delimiter;
	string del2 = "link";
	int linkNum;
	// write to output html
	if(myfile2.is_open()){
		int linetmp = FindLineOfLastStringInstance("id", inputhtmlFilename);
		if(linetmp == -1){cout<<"linetmp error"<<endl; return;}
		for(int i = 0; i < linetmp; i++) {
			getline (myfile2,line2);
			outfile<<line2<<endl;
		}
		size_t pos = line2.find(del2);
		if(pos==std::string::npos) {cout<<"cannot find link"<<endl; return;}
		string linkN = line2.substr(pos+del2.length(),line2.length()-1-pos-del2.length());
		linkNum = atoi(linkN.c_str()) + 1;

		getline(myfile2,line2);
		outfile<<line2<<endl;
	}
	else{
		cout<< "unable to open file" <<endl;
		return;
	}
	// append info to ouput html
	if (myfile.is_open()){
		while ( getline (myfile,line) ){
			size_t pos = line.find(del);
			if(pos==std::string::npos) {cout<<"cannot find"<<endl; return;}
			size_t pos2 = line.find(del,pos+1);
			if(pos2==std::string::npos) {cout<<"cannot find"<<endl; return;}

			string score = line.substr(pos2+del.length());
			if(atof(score.c_str())<threshold) continue;

			string cID1 = line.substr(0,pos);
			string cID2 = line.substr(pos+del.length(),pos2-pos-del.length());
			int sourceID, targetID;


			int tmp = FindByCommentID(cID1);
			if(tmp == -1){cout<<"cannot find comment id"<<endl; return;}
			string time1 = cTable[tmp]->timestamp;

			int tmp2 = FindByCommentID(cID2);
			if(tmp2 == -1){cout<<"cannot find comment id"<<endl; return;}
			string time2 = cTable[tmp2]->timestamp;

			if(time1.compare(time2)<0){
				sourceID = cTable[tmp]->intID;
				targetID = cTable[tmp2]->intID;

			}else{
				sourceID = cTable[tmp2]->intID;
				targetID = cTable[tmp]->intID;
			}

			char linkChar[5];
			string linkStr = std::to_string(linkNum);
			//itoa(linkNum, linkChar, 10);
			//string linkStr = string(linkChar);

			outfile<<","<<endl;
			outfile<<"{"<<endl;
			outfile<<"\"source\" : "<<sourceID<<","<<endl;
			outfile<<"\"target\" : "<<targetID<<","<<endl;
			outfile<<"\"class\" : \"assoc Destination\","<<endl;
			outfile<<"\"color\" : \"MediumAquamarine\","<<endl;
			outfile<<"\"id\" : \"link"+linkStr+"\""<<endl;
			outfile<<"}"<<endl;
			linkNum++;
		}
		myfile.close();
	}
	else cout << "Unable to open file";

	// write remaining lines
	while ( getline (myfile2,line2) ) outfile<<line2<<endl;

	myfile2.close();
	outfile.close();

	//PrintTable();
}

int parser::FindLineOfLastStringInstance(string s, string filename){
	int lineNum = -1;
	string line;
	ifstream myfile (filename.c_str());
	int linetmp = 0;
	if (myfile.is_open()){
		while ( getline (myfile,line) ){
			linetmp ++;
			size_t pos = line.find(s);
			if(pos!=std::string::npos) lineNum = linetmp;
		}
		myfile.close();
	}
	else cout << "Unable to open file";

	return lineNum;
}
